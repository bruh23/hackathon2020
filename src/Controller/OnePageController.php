<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OnePageController extends AbstractController
{
    /**
     * @Route("/one_page", name="one_page")
     */
    public function index()
    {
        return $this->render('one_page/index.html.twig', [
            'controller_name' => 'OnePageController',
        ]);
    }
}
