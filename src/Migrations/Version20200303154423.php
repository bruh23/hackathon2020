<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200303154423 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_account ADD date_debut_validation DATE NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD date_fin_validation DATE NOT NULL');
        $this->addSql('ALTER TABLE user_account DROP date_de_debut_validation');
        $this->addSql('ALTER TABLE user_account DROP date_de_fin_validation');
        $this->addSql('ALTER TABLE user_account ALTER date_de_naissance TYPE DATE');
        $this->addSql('ALTER TABLE user_account ALTER date_de_naissance DROP DEFAULT');
        $this->addSql('ALTER TABLE projet ALTER date_de_debut TYPE DATE');
        $this->addSql('ALTER TABLE projet ALTER date_de_debut DROP DEFAULT');
        $this->addSql('ALTER TABLE projet ALTER date_de_fin TYPE DATE');
        $this->addSql('ALTER TABLE projet ALTER date_de_fin DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE projet ALTER date_de_debut TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE projet ALTER date_de_debut DROP DEFAULT');
        $this->addSql('ALTER TABLE projet ALTER date_de_fin TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE projet ALTER date_de_fin DROP DEFAULT');
        $this->addSql('ALTER TABLE user_account ADD date_de_debut_validation TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD date_de_fin_validation TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_account DROP date_debut_validation');
        $this->addSql('ALTER TABLE user_account DROP date_fin_validation');
        $this->addSql('ALTER TABLE user_account ALTER date_de_naissance TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE user_account ALTER date_de_naissance DROP DEFAULT');
    }
}
