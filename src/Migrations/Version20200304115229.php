<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200304115229 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_account ADD nom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD prenom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD date_de_naissance DATE NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD adresse VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD porte_folio VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_account ADD cv VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD video VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD numero_de_tel VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD statut_de_validation BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD date_debut_validation DATE NOT NULL');
        $this->addSql('ALTER TABLE user_account ADD date_fin_validation DATE NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_account DROP nom');
        $this->addSql('ALTER TABLE user_account DROP prenom');
        $this->addSql('ALTER TABLE user_account DROP date_de_naissance');
        $this->addSql('ALTER TABLE user_account DROP adresse');
        $this->addSql('ALTER TABLE user_account DROP porte_folio');
        $this->addSql('ALTER TABLE user_account DROP cv');
        $this->addSql('ALTER TABLE user_account DROP video');
        $this->addSql('ALTER TABLE user_account DROP numero_de_tel');
        $this->addSql('ALTER TABLE user_account DROP statut_de_validation');
        $this->addSql('ALTER TABLE user_account DROP date_debut_validation');
        $this->addSql('ALTER TABLE user_account DROP date_fin_validation');
    }
}
