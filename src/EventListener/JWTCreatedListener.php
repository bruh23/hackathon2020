<?php

namespace App\EventListener;
// src/App/EventListener/JWTCreatedListener.php

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event){
        $data = $event->getData();
        $user = $event->getUser();

        $data['data'] = array(
            'nom' => $user->getNom(),
            'prenom' => $user->getPrenom(),
            'dateDeNaissance' => $user->getDateDeNaissance(),
            'adresse' => $user->getAdresse(),
            'porteFolio' => $user->getPorteFolio(),
            'numeroDeTel' => $user->getNumeroDeTel(),
        );

        $event->setData($data);
    }
}