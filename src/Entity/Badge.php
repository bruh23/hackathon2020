<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 */
class Badge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbDePointsRequis;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="badge")
     */
    private $userBadge;

    public function __construct()
    {
        $this->userBadge = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNbDePointsRequis(): ?int
    {
        return $this->nbDePointsRequis;
    }

    public function setNbDePointsRequis(int $nbDePointsRequis): self
    {
        $this->nbDePointsRequis = $nbDePointsRequis;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserBadge(): Collection
    {
        return $this->userBadge;
    }

    public function addUserBadge(User $userBadge): self
    {
        if (!$this->userBadge->contains($userBadge)) {
            $this->userBadge[] = $userBadge;
            $userBadge->addBadge($this);
        }

        return $this;
    }

    public function removeUserBadge(User $userBadge): self
    {
        if ($this->userBadge->contains($userBadge)) {
            $this->userBadge->removeElement($userBadge);
            $userBadge->removeBadge($this);
        }

        return $this;
    }
}
