<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CompetenceRepository")
 */
class Competence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Projet", mappedBy="competence")
     */
    private $projectCompetence;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Niveau", inversedBy="competenceLvl")
     */
    private $niveau;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="competence")
     */
    private $users;

    public function __construct()
    {
        $this->projectCompetence = new ArrayCollection();
        $this->niveau = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjectCompetence(): Collection
    {
        return $this->projectCompetence;
    }

    public function addProjectCompetence(Projet $projectCompetence): self
    {
        if (!$this->projectCompetence->contains($projectCompetence)) {
            $this->projectCompetence[] = $projectCompetence;
            $projectCompetence->addCompetence($this);
        }

        return $this;
    }

    public function removeProjectCompetence(Projet $projectCompetence): self
    {
        if ($this->projectCompetence->contains($projectCompetence)) {
            $this->projectCompetence->removeElement($projectCompetence);
            $projectCompetence->removeCompetence($this);
        }

        return $this;
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveau(): Collection
    {
        return $this->niveau;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveau->contains($niveau)) {
            $this->niveau[] = $niveau;
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        if ($this->niveau->contains($niveau)) {
            $this->niveau->removeElement($niveau);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addCompetence($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeCompetence($this);
        }

        return $this;
    }
}
