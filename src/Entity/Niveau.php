<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\NiveauRepository")
 */
class Niveau
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Competence", mappedBy="niveau")
     */
    private $competenceLvl;

    public function __construct()
    {
        $this->competenceLvl = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetenceLvl(): Collection
    {
        return $this->competenceLvl;
    }

    public function addCompetenceLvl(Competence $competenceLvl): self
    {
        if (!$this->competenceLvl->contains($competenceLvl)) {
            $this->competenceLvl[] = $competenceLvl;
            $competenceLvl->addNiveau($this);
        }

        return $this;
    }

    public function removeCompetenceLvl(Competence $competenceLvl): self
    {
        if ($this->competenceLvl->contains($competenceLvl)) {
            $this->competenceLvl->removeElement($competenceLvl);
            $competenceLvl->removeNiveau($this);
        }

        return $this;
    }
}
