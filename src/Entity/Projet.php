<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomDuClient;

    /**
     * @ORM\Column(type="integer")
     */
    private $remunerationParDev;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbPoints;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="projet")
     */
    private $projectUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Competence", inversedBy="projectCompetence")
     */
    private $competence;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDeDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDeFin;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="projet")
     */
    private $users;

    public function __construct()
    {
        $this->projectUser = new ArrayCollection();
        $this->competence = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNomDuClient(): ?string
    {
        return $this->nomDuClient;
    }

    public function setNomDuClient(?string $nomDuClient): self
    {
        $this->nomDuClient = $nomDuClient;

        return $this;
    }

    public function getRemunerationParDev(): ?int
    {
        return $this->remunerationParDev;
    }

    public function setRemunerationParDev(int $remunerationParDev): self
    {
        $this->remunerationParDev = $remunerationParDev;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getNbPoints(): ?int
    {
        return $this->nbPoints;
    }

    public function setNbPoints(int $nbPoints): self
    {
        $this->nbPoints = $nbPoints;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getProjectUser(): Collection
    {
        return $this->projectUser;
    }

    public function addProjectUser(User $projectUser): self
    {
        if (!$this->projectUser->contains($projectUser)) {
            $this->projectUser[] = $projectUser;
            $projectUser->addProjet($this);
        }

        return $this;
    }

    public function removeProjectUser(User $projectUser): self
    {
        if ($this->projectUser->contains($projectUser)) {
            $this->projectUser->removeElement($projectUser);
            $projectUser->removeProjet($this);
        }

        return $this;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetence(): Collection
    {
        return $this->competence;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competence->contains($competence)) {
            $this->competence[] = $competence;
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        if ($this->competence->contains($competence)) {
            $this->competence->removeElement($competence);
        }

        return $this;
    }

    public function getDateDeDebut(): ?\DateTimeInterface
    {
        return $this->dateDeDebut;
    }

    public function setDateDeDebut(\DateTimeInterface $dateDeDebut): self
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function getDateDeFin(): ?\DateTimeInterface
    {
        return $this->dateDeFin;
    }

    public function setDateDeFin(\DateTimeInterface $dateDeFin): self
    {
        $this->dateDeFin = $dateDeFin;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addProjet($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeProjet($this);
        }

        return $this;
    }
}
